﻿using UnityEngine;
using System.Collections;
using SimpleJSON;
using UnityEngine.UI;

public class GetMapel : MonoBehaviour {
    
    string urlMapel = "http://banksoalkompasilmu.com/api/pelajaran/2";
    public GameObject loading;
    public int[] id;
    public string[] mapel;
    public GameObject button;
    public RectTransform vLayout;

    void Start()
    {
        StartCoroutine(Mapel());
    }

    private IEnumerator Mapel()
    {
        string newUrl = urlMapel;
        WWW www = new WWW(newUrl);
        yield return www;
        if (www.error == null)
            ParseMapel(www.text);
        else
        {
            Application.LoadLevel("Home");
            Debug.Log(www.error);
        }

    }

    void ParseMapel(string datas)
    {
        var N = JSONNode.Parse(datas);

        id = new int[N["result"].Count];
        mapel = new string[N["result"].Count];
        if (DataUser.Instance.mapel.Length < N["result"].Count) DataUser.Instance.mapel = new string[N["result"].Count];
        if (DataUser.Instance.benarMapel.Length < N["result"].Count) DataUser.Instance.benarMapel = new float[N["result"].Count];
        if (DataUser.Instance.answer.Count < N["result"].Count) DataUser.Instance.newLengthAnswer(N["result"].Count);

        for (int i = 0; i < N["result"].Count; i++)
        {
            mapel[i] = N["result"][i]["pelajaran"];
            id[i] = N["result"][i]["id"].AsInt;
            DataUser.Instance.mapel[i] = N["result"][i]["pelajaran"];
        }

        vLayout.sizeDelta = new Vector2(vLayout.sizeDelta.x, (button.GetComponent<RectTransform>().sizeDelta.y * N["result"].Count) +
            (vLayout.GetComponent<VerticalLayoutGroup>().spacing * (N["result"].Count - 1)));

        for (int i = 0; i < N["result"].Count; i++)
        {
            ButtonMapel(button, mapel[i], i);
        }

        loading.SetActive(false);
    }

    public void ButtonMapel(GameObject button, string mapel, int id)
    {
        var newButton = Instantiate(button, vLayout.localPosition, Quaternion.identity) as GameObject;
        newButton.transform.parent = vLayout.transform;
        newButton.transform.localScale = new Vector3(1, 1, 1);
        newButton.GetComponentInChildren<Text>().text = mapel;
        newButton.GetComponent<IdMapel>().id = id;
    }
}
