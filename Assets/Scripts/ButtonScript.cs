﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using SimpleJSON;
using System.IO;
using System;

public class ButtonScript : MonoBehaviour {

    public GameObject soal;

	public string pembahasanUrl = "http://banksoalkompasilmu.com/api/download/";

	public void ChangeScene(string name)
    {
        SceneManager.LoadScene(name);
    }

    public void StartUN(int indexSoal)
    {
        soal.SetActive(true);
        soal.GetComponentInChildren<Camera>().gameObject.GetComponent<GameData>().StartUN(GetComponent<GetMapel>().id[indexSoal]);
        soal.GetComponentInChildren<Camera>().gameObject.GetComponent<GameData>().mapel = indexSoal;
        soal.GetComponentInChildren<Camera>().gameObject.GetComponent<GameData>().fieldMapel[0].text = GetComponent<GetMapel>().mapel[indexSoal];
        soal.GetComponentInChildren<Camera>().gameObject.GetComponent<GameData>().fieldMapel[1].text = GetComponent<GetMapel>().mapel[indexSoal];
        Destroy(gameObject);
	}

	public void downloadPembahasan(int id) {
		string savePath = Application.persistentDataPath;
		print (savePath);
		if (File.Exists (savePath + "/Pembahasan_SMP.pdf") && !isInternetConnection()) {
			string url = "file://"+(savePath + "/Pembahasan_SMP.pdf").Replace (" ", "%20");
			Application.OpenURL(url);
			return;
		}
		StartCoroutine (getLink (id));
	}

	IEnumerator getLink(int id) {
		print (pembahasanUrl + id);
		WWW www = new WWW(pembahasanUrl + id);
		yield return www;
		if (www.error == null)
		{
			StartCoroutine (Parse (www.text));
		}
		else
		{
			string savePath = Application.persistentDataPath;
			if (File.Exists (savePath + "/Pembahasan_SMP.pdf")) {
				string url = "file://"+(savePath + "/Pembahasan_SMP.pdf").Replace (" ", "%20");
				print (url);
				Application.OpenURL(url);
			}
			Application.LoadLevel("Home");
			Debug.Log(www.error);
		}
	}

	IEnumerator Parse(string data) {
		print (data);
		var N = JSONNode.Parse(data);
		var path = N ["link"];

		string savePath = Application.persistentDataPath;
		WWW www = new WWW(path);
		yield return www;

		byte[] bytes = www.bytes;
		try{
			File.WriteAllBytes(savePath+"/Pembahasan_SMP.pdf", bytes);
		}catch(Exception ex){
			Debug.Log (ex.Message);
		}
		print ("Open file pdf");
		if (File.Exists (savePath + "/Pembahasan_SMP.pdf")) {
			string url = "file://"+(savePath + "/Pembahasan_SMP.pdf").Replace (" ", "%20");
			print (url);
			Application.OpenURL(url);
		}
	}

	bool isInternetConnection()
	{
		bool isConnectedToInternet = false;
		if (Application.internetReachability == NetworkReachability.ReachableViaCarrierDataNetwork ||
			Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork)
		{
			isConnectedToInternet = true;
		}
		return isConnectedToInternet;
	}
}
