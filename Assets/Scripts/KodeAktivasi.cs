﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class KodeAktivasi : MonoBehaviour {

    public string kode;

    public InputField inputText;
    public Color textColor;

    void Awake()
    {
        if(PlayerPrefs.GetInt("CodeIsActive") == 1)
            Application.LoadLevel("Home");
    }

    public void nextBtn()
    {
        if(inputText.text == kode)
        {
            Application.LoadLevel("Home");
            PlayerPrefs.SetInt("CodeIsActive", 1);
        }
        else
        {
            inputText.GetComponent<Image>().color = Color.red;
            for (int i = 1; i < inputText.transform.childCount; i++)
            {
                inputText.transform.GetChild(i).GetComponent<Text>().color = Color.white;
            }
            StartCoroutine("backToWhite");
        }
    }

    IEnumerator backToWhite()
    {
        yield return new WaitForSeconds(0.5f);
        inputText.GetComponent<Image>().color = Color.white;
        for (int i = 1; i < inputText.transform.childCount; i++)
        {
            inputText.transform.GetChild(i).GetComponent<Text>().color = textColor;
        }
    }
}
