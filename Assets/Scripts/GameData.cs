﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using SimpleJSON;

public class GameData : MonoBehaviour
{
    public int mapel;
    string url = "http://banksoalkompasilmu.com/api/soal/";
    string urlGambarSoal = "http://banksoalkompasilmu.com/simulasiun/";
    public List<string> soal;
    public List<string> gambarSoal;
    public List<string> jawabanA;
    public List<string> jawabanB;
    public List<string> jawabanC;
    public List<string> jawabanD;
    public List<string> tipeJawbanA;
    public List<string> tipeJawbanB;
    public List<string> tipeJawbanC;
    public List<string> tipeJawbanD;
    public List<string> jawaban;
    public List<string> kunci;
    public Toggle[] pilihanJawaban;
    public Button[] buttonLembarJawab;
    [HideInInspector]
    public int nomor;
    public int totalSoal;
    float benar;
    float Hours = 2f;
    float Minutes = 00f;
    float Seconds = 00f;
    bool countDown;
    DataUser user;
    public Text fieldTime;
    public Text fieldSoal;
    public Text fieldNomor;
    public Text fieldHasil1;
    public Text fieldHasil2;
    public Text totalBenar;
    public Text totalSalah;
    public Text totalNilai;
    public Text kategori;
    public Text fieldPilihanA;
    public Text fieldPilihanB;
    public Text fieldPilihanC;
    public Text fieldPilihanD;
    public Text[] fieldMapel;
    public Text ketJawaban;
    public ToggleGroup groupToogle;
    public GameObject soalGame;
    public GameObject bottomSoal;
    public GameObject rekap;
    public GameObject bottomRekap;
    public GameObject loading;
    public GameObject popup;
    public GameObject layout;
    public GameObject hasil;
    public GameObject actionHasil;
    public RectTransform scrollSoal;
    public RectTransform scrollPilihan;
    public Image imgSoal;
    public Image imgPilihanA;
    public Image imgPilihanB;
    public Image imgPilihanC;
    public Image imgPilihanD;

    void Start()
    {
        fieldNomor.text = "Questions: 1";
        fieldTime.text = "SIsa Waktu : " + Rounded(Mathf.RoundToInt(Hours)) + " : " + Rounded(Mathf.RoundToInt(Minutes)) + " : " + Rounded(Mathf.RoundToInt(Seconds));
        user = GameObject.FindGameObjectWithTag("DataUser").GetComponent<DataUser>();
    }

    public void StartUN(int index)
    {
        StartCoroutine(GetSoal(index));
    }

    private IEnumerator GetSoal(int indexSoal)
    {
        string newUrl = url + indexSoal.ToString();
        WWW www = new WWW(newUrl);
        yield return www;
        if (www.error == null)
            Parse(www.text);
        else
        {
            Application.LoadLevel("Home");
            Debug.Log(www.error);
        }
    }

    public void ChangeSoal(int i)
    {
        popup.SetActive(false);
        nomor += i;
        imgSoal.sprite = null;

        if (nomor >= 0 && nomor <= totalSoal - 1)
        {
            fieldNomor.text = "Questions: " + (nomor + 1);
            fieldSoal.text = soal[nomor];
            if (gambarSoal[nomor] != "" && gambarSoal[nomor] != "null" && gambarSoal[nomor] != null && gambarSoal[nomor] != "http://banksoalkompasilmu.com/uploads")
            {
                StartCoroutine(setGambarSoal(gambarSoal[nomor]));
                imgSoal.gameObject.SetActive(true);
            }
            else
                imgSoal.gameObject.SetActive(false);
            if (tipeJawbanA[nomor] == "teks")
            {
                fieldPilihanA.text = jawabanA[nomor];
                imgPilihanA.gameObject.SetActive(false);
            }
            else
            {
                imgPilihanA.sprite = null;
                fieldPilihanA.text = "";
                imgPilihanA.gameObject.SetActive(true);
                StartCoroutine(setGambarPilihan(imgPilihanA, jawabanA[nomor]));
            }
            if (tipeJawbanB[nomor] == "teks")
            {
                fieldPilihanB.text = jawabanB[nomor];
                imgPilihanB.gameObject.SetActive(false);
            }
            else
            {
                imgPilihanB.sprite = null;
                fieldPilihanB.text = "";
                imgPilihanB.gameObject.SetActive(true);
                StartCoroutine(setGambarPilihan(imgPilihanB, jawabanB[nomor]));
            }
            if (tipeJawbanC[nomor] == "teks")
            {
                fieldPilihanC.text = jawabanC[nomor];
                imgPilihanC.gameObject.SetActive(false);
            }
            else
            {
                imgPilihanC.sprite = null;
                fieldPilihanC.text = "";
                imgPilihanC.gameObject.SetActive(true);
                StartCoroutine(setGambarPilihan(imgPilihanC, jawabanC[nomor]));
            }
            if (tipeJawbanD[nomor] == "teks")
            {
                fieldPilihanD.text = jawabanD[nomor];
                imgPilihanD.gameObject.SetActive(false);
            }
            else
            {
                imgPilihanD.sprite = null;
                fieldPilihanD.text = "";
                imgPilihanD.gameObject.SetActive(true);
                StartCoroutine(setGambarPilihan(imgPilihanD, jawabanD[nomor]));
            }
        }

        StartCoroutine(setContentHeight());

        if (nomor < 0) nomor = 0;
        if (nomor > totalSoal - 1) nomor = totalSoal - 1;

        if (jawaban[nomor] == "A")
        {
            pilihanJawaban[0].isOn = true;
            ketJawaban.text = "A";
        }
        else if (jawaban[nomor] == "B")
        {
            pilihanJawaban[1].isOn = true;
            ketJawaban.text = "B";
        }
        else if (jawaban[nomor] == "C")
        {
            pilihanJawaban[2].isOn = true;
            ketJawaban.text = "C";
        }
        else if (jawaban[nomor] == "D")
        {
            pilihanJawaban[3].isOn = true;
            ketJawaban.text = "D";
        }
        else if (jawaban[nomor] == "E")
        {
            pilihanJawaban[4].isOn = true;
            ketJawaban.text = "E";
        }
        else
        {
            groupToogle.SetAllTogglesOff();
            ketJawaban.text = "";
        }
    }

    public void Show()
    {
        rekap.SetActive(true);
        bottomRekap.SetActive(true);
        soalGame.SetActive(false);
        bottomSoal.SetActive(false);
        popup.SetActive(false);
        fieldHasil1.text = "";
        fieldHasil2.text = "";
        Debug.Log(jawaban.Count / 2);
        for (int i = 0; i < totalSoal; i++)
        {
            fieldHasil1.text += jawaban[i] + "\n";
        }

        for (int i = 25; i < jawaban.Count; i++)
        {
            fieldHasil2.text += jawaban[i] + "\n";
        }
    }

    void Update()
    {
        if (countDown)
        {
            Seconds -= Time.deltaTime;
            if (Seconds <= 0)
            {
                Seconds = 59f;
                if (Minutes > 0)
                {
                    Minutes--;
                }
                else if (Minutes <= 0 && Hours > 0)
                {
                    Minutes = 59f;
                    Hours--;
                }
            }
            fieldTime.text = "Sisa Waktu : " + Rounded(Mathf.RoundToInt(Hours)) + " : " + Rounded(Mathf.RoundToInt(Minutes)) + " : " + Rounded(Mathf.RoundToInt(Seconds));
        }

        if (Hours <= 0 && Minutes <= 0 && Seconds <= 0 && countDown)
        {
            countDown = false;
        }
    }

    public string Rounded(int n)
    {
        if (n < 10)
        {
            return "0" + n;
        }
        return n.ToString();
    }

    public void SetSoal(int n)
    {
        if(n<=totalSoal)
            nomor = n;
        Debug.Log(nomor);
        for (int i = 0; i < buttonLembarJawab.Length - 1; i++)
            if (i != nomor)
                buttonLembarJawab[i].interactable = true;
    }

    public void Ok()
    {
        soalGame.SetActive(true);
        bottomSoal.SetActive(true);
        rekap.SetActive(false);
        bottomRekap.SetActive(false);

        if (nomor >= 0 && nomor <= totalSoal - 1)
        {
            fieldNomor.text = "Questions: " + (nomor + 1);
            fieldSoal.text = soal[nomor];
            if (gambarSoal[nomor] != "" && gambarSoal[nomor] != "null" && gambarSoal[nomor] != null && gambarSoal[nomor] != "http://banksoalkompasilmu.com/uploads")
            {
                StartCoroutine(setGambarSoal(gambarSoal[nomor]));
                imgSoal.gameObject.SetActive(true);
            }
            else
                imgSoal.gameObject.SetActive(false);
            if (tipeJawbanA[nomor] == "teks")
            {
                fieldPilihanA.text = jawabanA[nomor];
                imgPilihanA.gameObject.SetActive(false);
            }
            else
            {
                imgPilihanA.sprite = null;
                fieldPilihanA.text = "";
                imgPilihanA.gameObject.SetActive(true);
                StartCoroutine(setGambarPilihan(imgPilihanA, jawabanA[nomor]));
            }
            if (tipeJawbanB[nomor] == "teks")
            {
                fieldPilihanB.text = jawabanB[nomor];
                imgPilihanB.gameObject.SetActive(false);
            }
            else
            {
                imgPilihanB.sprite = null;
                fieldPilihanB.text = "";
                imgPilihanB.gameObject.SetActive(true);
                StartCoroutine(setGambarPilihan(imgPilihanB, jawabanB[nomor]));
            }
            if (tipeJawbanC[nomor] == "teks")
            {
                fieldPilihanC.text = jawabanC[nomor];
                imgPilihanC.gameObject.SetActive(false);
            }
            else
            {
                imgPilihanC.sprite = null;
                fieldPilihanC.text = "";
                imgPilihanC.gameObject.SetActive(true);
                StartCoroutine(setGambarPilihan(imgPilihanC, jawabanC[nomor]));
            }
            if (tipeJawbanD[nomor] == "teks")
            {
                fieldPilihanD.text = jawabanD[nomor];
                imgPilihanD.gameObject.SetActive(false);
            }
            else
            {
                imgPilihanD.sprite = null;
                fieldPilihanD.text = "";
                imgPilihanD.gameObject.SetActive(true);
                StartCoroutine(setGambarPilihan(imgPilihanD, jawabanD[nomor]));
            }
        }
        StartCoroutine(setContentHeight());

        if (nomor < 0) nomor = 0;
        if (nomor > totalSoal - 1) nomor = totalSoal - 1;
        if (jawaban[nomor] == "A")
        {
            pilihanJawaban[0].isOn = true;
            ketJawaban.text = "A";
        }
        else if (jawaban[nomor] == "B")
        {
            pilihanJawaban[1].isOn = true;
            ketJawaban.text = "B";
        }
        else if (jawaban[nomor] == "C")
        {
            pilihanJawaban[2].isOn = true;
            ketJawaban.text = "C";
        }
        else if (jawaban[nomor] == "D")
        {
            pilihanJawaban[3].isOn = true;
            ketJawaban.text = "D";
        }
        else if (jawaban[nomor] == "E")
        {
            pilihanJawaban[4].isOn = true;
            ketJawaban.text = "E";
        }
        else
        {
            groupToogle.SetAllTogglesOff();
            ketJawaban.text = "";
        }

        for (int i = 0; i < buttonLembarJawab.Length - 1; i++)
            buttonLembarJawab[i].interactable = true;
    }

    public void Cancel()
    {
        soalGame.SetActive(true);
        bottomSoal.SetActive(true);
        rekap.SetActive(false);
        bottomRekap.SetActive(false);

        for (int i = 0; i < buttonLembarJawab.Length - 1; i++)
            buttonLembarJawab[i].interactable = true;
    }

    public void PopUp()
    {
        popup.SetActive(true);
    }

    public void CancelStop()
    {
        popup.SetActive(false);
    }

    public void Stop()
    {
        countDown = false;
        for (int i = 0; i < soal.Count; i++)
        {
            if (jawaban[i] == kunci[i])
                benar++;
        }
        user.benarMapel[mapel] = benar;
        totalBenar.text = "Jumlah Soal Benar\t\t\t\t\t\t\t\t\t\t: " + benar;
        totalSalah.text = "Jumlah Soal Salah\t\t\t\t\t\t\t\t\t\t: " + (totalSoal - benar);
        totalNilai.text = "Nilai UN\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t: " + (benar * ((float)100 / totalSoal));
        if ((benar * ((float)100 / totalSoal)) > 85)
            kategori.text = "KATEGORI NILAI\t\t\t\t\t\t: \"SANGAT BAIK\"";
        else if ((benar * ((float)100 / totalSoal)) > 70 && (benar * ((float)100 / totalSoal)) <= 85)
            kategori.text = "KATEGORI NILAI\t\t\t\t\t\t: \"BAIK\"";
        else if ((benar * ((float)100 / totalSoal)) > 55 && (benar * ((float)100 / totalSoal)) <= 70)
            kategori.text = "KATEGORI NILAI\t\t\t\t\t\t\t\t\t: \"CUKUP\"";
        else
            kategori.text = "KATEGORI NILAI\t\t\t\t\t\t\t\t: \"KURANG\"";
        Destroy(soalGame);
        Destroy(bottomSoal);
        Destroy(rekap);
        Destroy(bottomRekap);
        Destroy(layout);
        hasil.SetActive(true);
        actionHasil.SetActive(true);
    }

    void Parse(string datas)
    {
        var N = JSONNode.Parse(datas);

        for (int i = 0; i < N["result"].Count; i++)
        {
            soal.Add(N["result"][i]["soal"]);
            gambarSoal.Add(N["result"][i]["gambar"]);

            tipeJawbanA.Add(N["result"][i]["pilihan"][0]["tipe"]);
            tipeJawbanB.Add(N["result"][i]["pilihan"][1]["tipe"]);
            tipeJawbanC.Add(N["result"][i]["pilihan"][2]["tipe"]);
            tipeJawbanD.Add(N["result"][i]["pilihan"][3]["tipe"]);

            if (tipeJawbanA[i] == "teks")
                jawabanA.Add(N["result"][i]["pilihan"][0]["jawaban"]);
            else
                jawabanA.Add(N["result"][i]["pilihan"][0]["gambar"]);

            if (tipeJawbanB[i] == "teks")
                jawabanB.Add(N["result"][i]["pilihan"][1]["jawaban"]);
            else
                jawabanB.Add(N["result"][i]["pilihan"][1]["gambar"]);

            if (tipeJawbanC[i] == "teks")
                jawabanC.Add(N["result"][i]["pilihan"][2]["jawaban"]);
            else
                jawabanC.Add(N["result"][i]["pilihan"][2]["gambar"]);

            if (tipeJawbanD[i] == "teks")
                jawabanD.Add(N["result"][i]["pilihan"][3]["jawaban"]);
            else
                jawabanD.Add(N["result"][i]["pilihan"][3]["gambar"]);

            for (int p = 0; p < N["result"][i]["pilihan"].Count; p++)
                if (N["result"][i]["pilihan"][p]["is_kunci"].AsInt > 0)
                    switch (p)
                    {
                        case 0:
                            kunci.Add("A");
                            break;
                        case 1:
                            kunci.Add("B");
                            break;
                        case 2:
                            kunci.Add("C");
                            break;
                        case 3:
                            kunci.Add("D");
                            break;
                        case 4:
                            kunci.Add("E");
                            break;
                    }
        }
        totalSoal = soal.Count;
        var arr = new string[totalSoal];
        jawaban = new List<string>(arr);
        user.answer[mapel].jawaban = new List<string>(arr);
        //if (mapel == 0) user.jawabanMapel1 = new string[totalSoal];
        //else if (mapel == 1) user.jawabanMapel2 = new string[totalSoal];
        //else if (mapel == 2) user.jawabanMapel3 = new string[totalSoal];
        //else if (mapel == 3) user.jawabanMapel4 = new string[totalSoal];
        //else if (mapel == 4) user.jawabanMapel5 = new string[totalSoal];
        //else if (mapel == 5) user.jawabanMapel6 = new string[totalSoal];

        fieldSoal.text = soal[nomor];
        if (gambarSoal[nomor] != "" && gambarSoal[nomor] != "null" && gambarSoal[nomor] != null && gambarSoal[nomor] != "http://banksoalkompasilmu.com/uploads")
        {
            StartCoroutine(setGambarSoal(gambarSoal[nomor]));
            imgSoal.gameObject.SetActive(true);
        }
        else
            imgSoal.gameObject.SetActive(false);
        if (tipeJawbanA[nomor] == "teks")
        {
            fieldPilihanA.text = jawabanA[nomor];
            imgPilihanA.gameObject.SetActive(false);
        }
        else
        {
            imgPilihanA.sprite = null;
            fieldPilihanA.text = "";
            imgPilihanA.gameObject.SetActive(true);
            StartCoroutine(setGambarPilihan(imgPilihanA, jawabanA[nomor]));
        }
        if (tipeJawbanB[nomor] == "teks")
        {
            fieldPilihanB.text = jawabanB[nomor];
            imgPilihanB.gameObject.SetActive(false);
        }
        else
        {
            imgPilihanB.sprite = null;
            fieldPilihanB.text = "";
            imgPilihanB.gameObject.SetActive(true);
            StartCoroutine(setGambarPilihan(imgPilihanB, jawabanB[nomor]));
        }
        if (tipeJawbanC[nomor] == "teks")
        {
            fieldPilihanC.text = jawabanC[nomor];
            imgPilihanC.gameObject.SetActive(false);
        }
        else
        {
            imgPilihanC.sprite = null;
            fieldPilihanC.text = "";
            imgPilihanC.gameObject.SetActive(true);
            StartCoroutine(setGambarPilihan(imgPilihanC, jawabanC[nomor]));
        }
        if (tipeJawbanD[nomor] == "teks")
        {
            fieldPilihanD.text = jawabanD[nomor];
            imgPilihanD.gameObject.SetActive(false);
        }
        else
        {
            imgPilihanD.sprite = null;
            fieldPilihanD.text = "";
            imgPilihanD.gameObject.SetActive(true);
            StartCoroutine(setGambarPilihan(imgPilihanD, jawabanD[nomor]));
        }

        StartCoroutine(setContentHeight());
        countDown = true;
        Destroy(loading);
    }

    IEnumerator setContentHeight()
    {
        yield return new WaitForSeconds(0.0f);
        float newHeightSoal;
        float newHeightPilihan;
        float newPos;
        //sroll soal height
        if (gambarSoal[nomor] != "" && gambarSoal[nomor] != "null" && gambarSoal[nomor] != null && gambarSoal[nomor] != "http://banksoalkompasilmu.com/uploads")
        {
            newHeightSoal = fieldSoal.rectTransform.sizeDelta.y + 217f;
            scrollSoal.sizeDelta = new Vector2(scrollSoal.sizeDelta.x, newHeightSoal);
            newPos = fieldSoal.rectTransform.localPosition.y - fieldSoal.rectTransform.sizeDelta.y;
            imgSoal.rectTransform.localPosition = new Vector3(imgSoal.rectTransform.localPosition.x, newPos, imgSoal.rectTransform.localPosition.z);
        }
        else
        {
            newHeightSoal = fieldSoal.rectTransform.sizeDelta.y;
            scrollSoal.sizeDelta = new Vector2(scrollSoal.sizeDelta.x, newHeightSoal);
        }
        scrollSoal.localPosition = new Vector3(scrollSoal.localPosition.x, 0, scrollSoal.localPosition.z);
        //scroll pilihan height
        if (tipeJawbanA[nomor] == "gambar")
        {
            newHeightPilihan = 314.4f;
            scrollPilihan.sizeDelta = new Vector2(scrollPilihan.sizeDelta.x, newHeightPilihan);
        }
        else
        {
            newHeightPilihan = (fieldPilihanA.rectTransform.sizeDelta.y + fieldPilihanB.rectTransform.sizeDelta.y +
                fieldPilihanC.rectTransform.sizeDelta.y + fieldPilihanD.rectTransform.sizeDelta.y) + (15 * 5);
            scrollPilihan.sizeDelta = new Vector2(scrollPilihan.sizeDelta.x, newHeightPilihan);
        }
    }

    IEnumerator setGambarSoal(string urlGambar)
    {
        WWW www = new WWW(urlGambar);
        yield return www;

        if (www.error == null)
            if (gambarSoal[nomor] != "" && gambarSoal[nomor] != "null" && gambarSoal[nomor] != null && gambarSoal[nomor] != "http://banksoalkompasilmu.com/uploads")
            {
                float newWidth;
                float newHeight;
                float newHeightSoal;
                imgSoal.rectTransform.sizeDelta = new Vector2(217f, 217f);
                imgSoal.sprite = Sprite.Create(www.texture,
                    new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0.5f, 0.5f));

                if (www.texture.width > www.texture.height)
                {
                    newHeight = (imgSoal.rectTransform.sizeDelta.x * www.texture.height) / www.texture.width;
                    imgSoal.rectTransform.sizeDelta = new Vector2(imgSoal.rectTransform.sizeDelta.x, newHeight);
                    newHeightSoal = fieldSoal.rectTransform.sizeDelta.y + imgSoal.rectTransform.sizeDelta.y;
                    scrollSoal.sizeDelta = new Vector2(scrollSoal.sizeDelta.x, newHeightSoal);
                }
                else
                {
                    newWidth = (imgSoal.rectTransform.sizeDelta.y * www.texture.width) / www.texture.height;
                    imgSoal.rectTransform.sizeDelta = new Vector2(newWidth, imgSoal.rectTransform.sizeDelta.y);
                    newHeightSoal = fieldSoal.rectTransform.sizeDelta.y + imgSoal.rectTransform.sizeDelta.y;
                    scrollSoal.sizeDelta = new Vector2(scrollSoal.sizeDelta.x, newHeightSoal);
                }
            }
        
    }

    IEnumerator setGambarPilihan(Image img, string urlGambar)
    {
        WWW www = new WWW(urlGambar);
        yield return www;
        if (www.error == null)
        {
            float newWidth;
            float newHeight;
            img.sprite = Sprite.Create(www.texture,
                    new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0.5f, 0.5f));

            if (www.texture.width > www.texture.height)
            {
                newHeight = (106f * www.texture.height) / www.texture.width;
                img.rectTransform.sizeDelta = new Vector2(106f, newHeight);
            }
            else
            {
                newWidth = (106f * www.texture.width) / www.texture.height;
                img.rectTransform.sizeDelta = new Vector2(newWidth, 106f);
            }
        }
    }
}
